#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>

#define MAX_ROWS 1024
#define MAX_COLS 1024

// buffer function begin
void editor_buffer(const char* buffer, const size_t buffer_size) { 
    struct winsize ws; 							 //
    if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) == -1 || ws.ws_col == 0) { // grab window size
        exit(EXIT_FAILURE);						 // 
    }

    size_t rows = ws.ws_row - 1; // number of rows and columns to show
    size_t cols = ws.ws_col;     //

    char lines[MAX_ROWS][MAX_COLS]; // store text in an array
    size_t line_count = 0; 
    size_t line_length = 0; 

    // buffer loop begin
    for (size_t i = 0; i < buffer_size; i++) { 
        char c = buffer[i]; 
        if (c == '\n' || line_length == cols - 1) { // if the end of the line is reached
            lines[line_count][line_length] = '\0';  // null terminator, 
            line_count++;                           // add to line count,
            line_length = 0;                        // and reset line length.
        } else {
            lines[line_count][line_length] = c;// add character to line 
            line_length++;                     // add to line length
        }
    } 
    // buffer loop end

    if (line_length > 0) { 			//if last line isn't terminated
        lines[line_count][line_length] = '\0';  // null terminator
        line_count++; 				// add to line count
    }

    for (size_t i = 0; i < rows; i++) { 
        if (i < line_count) {		// if there are lines to show
            printf("%s\n", lines[i]);   // show them
        } else {			// else
            printf("~\n"); 		// print a pretty tilda
        }
    }
}
// buffer function end

char* read_file(const char* filename, size_t* buffer_size) {
    FILE* file = fopen(filename, "r"); 
    if (!file) { 
        return NULL; 
    }

    fseek(file, 0, SEEK_END); 
    *buffer_size = ftell(file); 
    fseek(file, 0, SEEK_SET); 

    char* buffer = (char*)malloc(*buffer_size); 
    if (!buffer) { 
        fclose(file); 
        return NULL; 
    }

    size_t bytes_read = fread(buffer, 1, *buffer_size, file); 
    fclose(file); 
    if (bytes_read != *buffer_size) { 
        free(buffer); 
        return NULL; 
    }

    return buffer; 
}

int main(int argc, char* argv[]) {
    if (argc != 2) { 
        printf("Usage: %s <filename>\n", argv[0]); 
        return 1; 
    }

    size_t buffer_size; 
    char* buffer = read_file(argv[1], &buffer_size); 
    if (!buffer) { 
        printf("Error: could not read file '%s'\n", argv[1]); 
        return 1; 
    }

    editor_buffer(buffer, buffer_size); 

    free(buffer); 
    return 0; 
}
